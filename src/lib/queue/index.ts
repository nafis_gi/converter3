import { Executor, State, Task } from "./types";

class Queue<T> {
  private count = 0;
  private readonly waiting: string[] = [];
  private readonly parallels: number;
  private readonly executor: Executor<T>;
  private readonly state: State<T> = new Map();

  constructor(parallels: number, executor: Executor<T>) {
    this.executor = executor;
    this.parallels = parallels;
  }

  add({ task, token }: Task<T>) {
    this.state.set(token, {
      task,
      status: "waiting",
    });
    if (this.count < this.parallels) {
      this.next(token);
    } else {
      this.waiting.push(token);
    }
  }

  private async next(token: string) {
    this.count++;
    const stateItem = this.state.get(token);
    const task = stateItem?.task;

    if (!task) {
      throw new Error("Task not found");
    }

    try {
      await this.executor({ token, task });
      this.state.set(token, {
        task,
        status: "success",
      });
    } catch (error) {
      this.state.set(token, {
        task,
        status: "error",
      });
    }

    this.count--;

    const nextTask = this.waiting.shift();
    nextTask && this.next(nextTask);
  }

  getStatus(token: string) {
    return this.state.get(token)?.status || "not-found";
  }
}

export { Queue };
