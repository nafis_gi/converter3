export type Task<T> = {
  token: string;
  task: T;
};

export type Executor<T> = (input: Task<T>) => Promise<unknown>;

const STATUSES = ["error", "success", "waiting"] as const;

export type Status = (typeof STATUSES)[number];

export type State<T> = Map<
  string,
  {
    task: T;
    status: Status;
  }
>;
