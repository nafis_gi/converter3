export class History {
  private refreshTime: number;
  private limit: number;
  private state: Map<string, Record<number, number>> = new Map();

  constructor(limit: number, refreshTime: number) {
    this.limit = limit;
    this.refreshTime = refreshTime;
  }

  add(id: string, time: Date, count: number) {
    const userHistory = this.state.get(id) || {};
    userHistory[new Date(time).getTime()] = count;

    this.state.set(id, userHistory);
  }

  check(id: string, count: number) {
    const userHistory = this.state.get(id);
    if (!userHistory) {
      return true;
    }

    const totalCount = Object.entries(userHistory).reduce((result, item) => {
      const [time, count] = item;
      if (Date.now() - Number(time) < this.refreshTime) {
        result += count;
      }
      return result;
    }, 0);

    return this.limit - totalCount >= count;
  }

  clear(id: string) {
    this.state.delete(id);
  }
}
