export const GOOGLE_CLIENT_ID =
  "37526554440-cam9oofdumbtncpk82gmr2dans19maes.apps.googleusercontent.com";
export const GOOGLE_CLIENT_SECRET = "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";
export const GOOGLE_CALLBACK_URL = "http://localhost:3000/auth/google/callback";
export const COOKIE_SECRET = "cookie-sectert-dsfmdklgmfdkl";

export const UPLOAD_DIR = "./uploads";

export const FREE_MIMETYPES = ["image/jpeg"];

export const ROUTES = {
  main: "/",
  googleAuth: "/auth/google",
  googleAuthCallback: "/auth/google/callback",
  logout: "/auth/logout",
  login: "/login",
  fileUpload: "/files/upload",
  fileGetStatus: "/files/:token",
  pay: "/pay",
};

export const UNAUTH_USER_REFRESH_TIME = 30 * 24 * 60 * 60 * 1000; // Месяц в миллисекундах
export const AUTH_USER_REFRESH_TIME = 24 * 60 * 60 * 1000; // День в миллисекундах
export const PAID_USER_REFRESH_TIME = 24 * 60 * 60 * 1000; // День в миллисекундах

export const UNAUTH_USER_LIMIT = 20;
export const AUTH_USER_LIMIT = 20;
export const PAID_USER_LIMIT = 200;

export const AVAILABLE_FORMATS = [
  "avif",
  "dz",
  "fits",
  "gif",
  "heif",
  "input",
  "jpeg",
  "jpg",
  "jp2",
  "jxl",
  "magick",
  "openslide",
  "pdf",
  "png",
  "ppm",
  "raw",
  "svg",
  "tiff",
  "tif",
  "v",
  "webp",
];
