import { Router } from "express";
import { ROUTES } from "@src/config";
import { addToState, authMiddleware, useHistory } from "@src/services";

const MONTH = 30 * 24 * 60 * 60 * 1000;

export const payRouter = Router();

payRouter.get(ROUTES.pay, authMiddleware, useHistory, (req, res) => {
  const userId = req.user?.id || "";
  addToState(userId, Date.now() + MONTH);
  req.history.clear(userId);
  res.send("Покупка прошла успешно");
});
