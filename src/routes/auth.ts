import { Router } from "express";
import {
  googleAuthHandler,
  googleAuthCallbackHandler,
  logoutHandler,
  authMiddleware,
} from "@src/services";
import { ROUTES } from "@src/config";

export const authRouter = Router();

authRouter.get(ROUTES.login, authMiddleware, (req, res) => {
  res.send("Вы авторизованы");
});

authRouter.get(ROUTES.googleAuth, googleAuthHandler);

authRouter.get(ROUTES.googleAuthCallback, googleAuthCallbackHandler);

authRouter.get(ROUTES.logout, logoutHandler);
