import { Router } from "express";
import { ROUTES } from "@src/config";
import { authRouter } from "./auth";
import { filesRouter } from "./files";
import { payRouter } from "./payment";

export const router = Router();
router.get(ROUTES.main, (req, res) => {
  const authorized = req.isAuthenticated();
  res.send(
    authorized
      ? `Привет, ${req.user?.name.givenName}`
      : "Привет, неавторизованный пользователь!"
  );
});

router.use(authRouter);
router.use(filesRouter);
router.use(payRouter);
