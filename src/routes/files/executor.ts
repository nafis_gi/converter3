import { OutputInfo } from "sharp";
import { converter } from "@src/services/converter";
import { ConverterQueueTask } from "./types"
import { Task } from "@src/lib/queue/types";

export const executor = async ({
  token,
  task,
}: Task<ConverterQueueTask>) => {
  const result: OutputInfo[] = [];
  for (const file of task.files) {
    result.push(await converter(file, token, task?.format));
  }
  return result;
};

