import { NextFunction, Request, Response } from "express";
import mimeTypes from "mime-types";

export const usePrepareFiles = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  let files: Express.Multer.File[] = [];
  if (req.files) {
    if (Array.isArray(req.files)) {
      files = req.files;
    } else {
      Object.values(req.files).forEach((items) => {
        files = files.concat(items);
      });
    }
  }

  const images = files.filter((file) => {
    const mimeType = mimeTypes.lookup(file.originalname);
    return mimeType && mimeType.startsWith("image/");
  });
  req.images = images;
  next();
};
