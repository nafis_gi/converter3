import { NextFunction, Request, Response } from "express";
import { FREE_MIMETYPES } from "@src/config";

export const useValidate = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const authorized = req.isAuthenticated();

  if (
    !authorized &&
    !req.images?.every((file) => FREE_MIMETYPES.includes(file.mimetype))
  ) {
    res.json({
      message: `Нужно авторизоваться. Бесплатно можно конвертировать только ${FREE_MIMETYPES.join(
        ", "
      )} файлы`,
    });
  }
  next();
};
