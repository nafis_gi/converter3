import { Express } from "express";
import { FormatEnum } from "sharp";
import { Task } from "@src/lib/queue/types";

export type ConverterQueueTask = {
  files: Express.Multer.File[];
  format?: keyof FormatEnum;
};
