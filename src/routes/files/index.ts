import { Request, Response, Router } from "express";
import multer from "multer";
import { v4 as uuid } from "uuid";
import { FormatEnum } from "sharp";

import { AVAILABLE_FORMATS, ROUTES, UPLOAD_DIR } from "@src/config";
import { Queue } from "@src/lib";
import { useSubscriptionUser, useHistory } from "@src/services";

import { executor } from "./executor";
import { usePrepareFiles, useValidate } from "./hooks";
import { ConverterQueueTask } from "./types";

export const filesRouter = Router();
const upload = multer({ dest: UPLOAD_DIR });

const freeQueue = new Queue<ConverterQueueTask>(1, executor);
const paidQueue = new Queue<ConverterQueueTask>(5, executor);

filesRouter.post(
  ROUTES.fileUpload,
  upload.array("files"),
  usePrepareFiles,
  useValidate,
  useSubscriptionUser,
  useHistory,
  (req, res) => {
    const token = uuid();
    const authorized = req.isAuthenticated();
    const { images = [], query } = req;
    let toFormat: keyof FormatEnum | undefined;

    if (
      query.format &&
      typeof query.format === "string" &&
      AVAILABLE_FORMATS.includes(query.format)
    ) {
      toFormat = query.format as keyof FormatEnum;
    }

    const userId = authorized ? req.user?.id : req.sessionID;
    const queue = req.user?.privileged ? paidQueue : freeQueue;

    if (req.history.check(userId, images.length)) {
      req.history.add(userId, new Date(), images.length);
      queue.add({
        token,
        task: {
          files: images,
          format: toFormat,
        },
      });
    } else {
      res.json({ message: "Лимит превышен" });
    }

    res.json({ token });
  }
);

filesRouter.get(ROUTES.fileGetStatus, (req: Request, res: Response) => {
  const result = freeQueue.getStatus(req.params.token);
  res.json(result ? result : { status: "NOTFOUND" });
});
