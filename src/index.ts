import express from "express";
import passport from "passport";
import { cookieParserMiddleware } from "./services";
import session from "express-session";

import { router } from "./routes";

const app = express();

app.set("view engine", "ejs");
app.use(
  session({
    secret: "secret",
    resave: true,
    saveUninitialized: true,
  })
);
app.use(cookieParserMiddleware);
app.use(passport.initialize());
app.use(passport.session());
app.use(router);
app.use(express.static("uploads"));

app.listen(3000, () => {
  console.log("Сервер запущен на порту 3000");
});
