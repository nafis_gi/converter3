import { NextFunction, Request, Response } from "express";

const subscriptionState: Map<string, number> = new Map();

export const addToState = (userId: string, endTime: number) => {
  subscriptionState.set(userId, endTime);
};

export const useSubscriptionUser = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (req.isAuthenticated()) {
    const userSubscriptionEndTime = subscriptionState.get(req.user.id);

    if (
      userSubscriptionEndTime &&
      Date.now() < new Date(userSubscriptionEndTime).getTime()
    ) {
      req.user.privileged = true;
    }
  }
  next();
};
