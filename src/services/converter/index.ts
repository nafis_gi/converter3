import type { FormatEnum, OutputInfo } from "sharp";
import sharp from "sharp";
import fs from "fs";
import path from "path";
import { UPLOAD_DIR } from "@src/config";

export const converter = (
  file: Express.Multer.File,
  token: string,
  toFormat: keyof FormatEnum = "webp"
) => {
  const resultDir = `${UPLOAD_DIR}/${token}`;
  if (!fs.existsSync(resultDir)) {
    fs.mkdirSync(resultDir);
  }

  return new Promise<OutputInfo>((resolve, reject) => {
    sharp(file.path)
      .toFormat(toFormat)
      .toFile(
        `${resultDir}/${path.parse(file.originalname).name}.${toFormat}`,
        (err, info) => {
          if (err) {
            reject(err);
          }
          console.log(`${file.originalname} converted to ${info.format}`);
          resolve(info);
        }
      );
  });
};
