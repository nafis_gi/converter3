import { Request, Response, NextFunction } from "express";

import {
  AUTH_USER_LIMIT,
  AUTH_USER_REFRESH_TIME,
  PAID_USER_LIMIT,
  PAID_USER_REFRESH_TIME,
  UNAUTH_USER_LIMIT,
  UNAUTH_USER_REFRESH_TIME,
} from "@src/config";
import { History } from "@src/lib";

const unauthUsersHistory = new History(
  UNAUTH_USER_LIMIT,
  UNAUTH_USER_REFRESH_TIME
);
const authUsersHistory = new History(AUTH_USER_LIMIT, AUTH_USER_REFRESH_TIME);
const privilegedUsersHistory = new History(
  PAID_USER_LIMIT,
  PAID_USER_REFRESH_TIME
);

export const useHistory = (req: Request, res: Response, next: NextFunction) => {
  if (req.isAuthenticated()) {
    if (req.user?.privileged) {
      req.history = privilegedUsersHistory;
    } else {
      req.history = authUsersHistory;
    }
  } else {
    req.history = unauthUsersHistory;
  }
  next();
};
