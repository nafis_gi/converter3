import passport from "passport";
import { Strategy as GoogleStrategy } from "passport-google-oauth20";
import { Request, Response, NextFunction } from "express";
import cookieParser from "cookie-parser";
import {
  GOOGLE_CLIENT_ID,
  GOOGLE_CLIENT_SECRET,
  GOOGLE_CALLBACK_URL,
  COOKIE_SECRET,
  ROUTES,
} from "../../config";

passport.serializeUser((user: any, done) => {
  done(null, user);
});

passport.deserializeUser((user: any, done) => {
  done(null, user);
});

passport.use(
  new GoogleStrategy(
    {
      clientID: GOOGLE_CLIENT_ID,
      clientSecret: GOOGLE_CLIENT_SECRET,
      callbackURL: GOOGLE_CALLBACK_URL,
    },
    (accessToken, refreshToken, profile: any, done) => {
      return done(null, profile);
    }
  )
);

export const authMiddleware = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect(ROUTES.googleAuth);
};

export const googleAuthHandler = passport.authenticate("google", {
  scope: ["profile", "email"],
});

export const googleAuthCallbackHandler = passport.authenticate("google", {
  successRedirect: ROUTES.main,
  failureRedirect: ROUTES.login,
});

export const logoutHandler = (req: Request, res: Response) => {
  req.logout((err) => {
    console.error(err);
  });
  res.redirect(ROUTES.main);
};

export const cookieParserMiddleware = cookieParser(COOKIE_SECRET);
