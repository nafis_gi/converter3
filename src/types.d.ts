import { Express } from "express";
import { History } from "./lib";

declare global {
  namespace Express {
    interface Request {
      images?: Express.Multer.File[];
      history: History;
    }
    interface User {
      id: string;
      displayName: string;
      name: {
        familyName: string;
        givenName: string;
      };
      privileged?: boolean;
    }
  }
}
