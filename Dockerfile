FROM node:19.9.0

RUN apt-get update -y && \
    apt-get upgrade -y && \
    npm i -g npm --upgrade && \
    npm i -g pm2 sharp

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY . .

RUN npm i && npm run compile

ENV HOST 0.0.0.0
EXPOSE 3000

CMD ["pm2-runtime", "build/src/index.js"]